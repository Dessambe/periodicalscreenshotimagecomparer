﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PeriodicalScreenShotImageComparer.ViewModels;

namespace PeriodicalScreenShotImageComparer.Views
{
    /// <summary>
    /// Interaction logic for ScreenShotWindow.xaml
    /// </summary>
    public partial class ScreenShotWindow : Window
    {
        private bool _isDragging = false;
        private Point _anchorPoint = new Point();

        public ScreenShotWindow()
        {
            InitializeComponent();
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _anchorPoint.X = e.GetPosition(CanvasElement).X;
            _anchorPoint.Y = e.GetPosition(CanvasElement).Y;
            _isDragging = true;
        }

        private void Image_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isDragging)
            {
                double x = e.GetPosition(CanvasElement).X;
                double y = e.GetPosition(CanvasElement).Y;
                
                Rect.SetValue(Canvas.LeftProperty, Math.Min(x, _anchorPoint.X));
                Rect.SetValue(Canvas.TopProperty, Math.Min(y, _anchorPoint.Y));

                Rect.Width = Math.Abs(x - _anchorPoint.X);
                Rect.Height = Math.Abs(y - _anchorPoint.Y);

                if (Rect.Visibility != Visibility.Visible)
                    Rect.Visibility = Visibility.Visible;
            }
        }

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _isDragging = false;
        }

        private void Image_MouseLeave(object sender, MouseEventArgs e)
        {
            _isDragging = false;
        }
    }
}
