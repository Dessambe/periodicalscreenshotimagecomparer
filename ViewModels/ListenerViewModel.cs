﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PeriodicalScreenShotImageComparer.Views;
using System.Windows;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Input;
using PeriodicalScreenShotImageComparer.Commands;
using System.ComponentModel;
using System.IO;
using System.Media;
using System.Threading;
using PeriodicalScreenShotImageComparer.Properties;

namespace PeriodicalScreenShotImageComparer.ViewModels
{
    public class ListenerViewModel : BaseViewModel
    {
        #region Binding: Image
        private ImageSource _croppedImage;
        public ImageSource CroppedImage
        {
            get { return _croppedImage; }
            set { _croppedImage = value; OnPropertyChanged("CroppedImage"); }
        }

        private ImageSource _comparedImage;
        public ImageSource ComparedImage
        {
            get { return _comparedImage; }
            set { _comparedImage = value; OnPropertyChanged("ComparedImage"); }
        }
        #endregion

        #region Binding: BackgroundColor
        private System.Windows.Media.Brush _background;
        public System.Windows.Media.Brush Background
        {
            get { return _background; }
            set { _background = value; OnPropertyChanged("Background"); }
        }
        #endregion

        #region Binding: Visibility of Start Button
        private Visibility _startButton;
        public Visibility StartButton
        {
            get { return _startButton; }
            set { _startButton = value; OnPropertyChanged("StartButton"); }
        }
        #endregion

        #region Binding: Visibility of Stop Button
        private Visibility _stopButton;
        public Visibility StopButton
        {
            get { return _stopButton; }
            set { _stopButton = value; OnPropertyChanged("StopButton"); }
        }
        #endregion

        #region Commands
        private ICommand _listenCommand;
        public ICommand ListenCommand
        {
            get { return _listenCommand ?? (_listenCommand = new BaseCommand(StartListening, () => ListenCommandCanExecute())); }
        }

        private bool ListenCommandCanExecute()
        {
            if (CroppedImage != null)
                return true;
            else
                return false;
        }

        private ICommand _copyFromClipboardCommand;
        public ICommand CopyFromClipboardCommand
        {
            get { return _copyFromClipboardCommand ?? (_copyFromClipboardCommand = new BaseCommand(CopyFromClipboard)); }
        }

        private ICommand _showOptionsWindowCommand;
        public ICommand ShowOptionsWindowCommand
        {
            get { return _showOptionsWindowCommand ?? (_showOptionsWindowCommand = new BaseCommand(OpenOptionsWindow)); }
        }
        #endregion

        private ICommand _stopCommand;
        public ICommand StopCommand
        {
            get { return _stopCommand ?? (_stopCommand = new BaseCommand(StopListening)); }
        }

        #region Fields
        private int CanvasLeft, CanvasTop, RectWidth, RectHeight;
        private Bitmap comparedBitmap, croppedBitmap;
        Bitmap bitImage;
        ImageSource ImageFromClip;
        BackgroundWorker worker;
        OptionsWindowViewModel optionsWindowViewModel;
        #endregion

        public ListenerViewModel()
        {
            AssignButtonsVisibility();
            AssignDefaultVoice();
            Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 146, 119, 172));
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += worker_DoWork;
        }

        private void AssignButtonsVisibility()
        {
            StartButton = Visibility.Visible;
            StopButton = Visibility.Collapsed;
        }

        private void AssignDefaultVoice()
        {
            if (String.IsNullOrEmpty(Settings.Default.VoiceFile))
            {
                Settings.Default.VoiceFile = Directory.GetCurrentDirectory() + @"\Sounds\voice.wav";
            }
        }

        public void OpenOptionsWindow()
        {
            optionsWindowViewModel = new OptionsWindowViewModel(this);
            OptionsWindow optionsWindow = new OptionsWindow();
            optionsWindow.DataContext = optionsWindowViewModel;
            optionsWindow.ShowDialog();
        }

        public void GetCroppedImage(Bitmap croppedImage, int CanvasLeft, int CanvasTop, int RectWidth, int RectHeight)
        {
            this.croppedBitmap = croppedImage;
            CroppedImage = ConvertBitmap(croppedImage);

            this.CanvasLeft = CanvasLeft;
            this.CanvasTop = CanvasTop;
            this.RectWidth = RectWidth;
            this.RectHeight = RectHeight;

            Console.WriteLine(CroppedImage.ToString());
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {                
                System.Threading.Thread.Sleep(new TimeSpan(0,0,Settings.Default.TimeInterval));
                if (worker.CancellationPending)
                {
                    return;
                }
                CaptureFromScreen();
            }
        }

        public void StartListening()
        {
            worker.RunWorkerAsync();
            StartButton = Visibility.Collapsed;
            StopButton = Visibility.Visible;
        }

        public void StopListening()
        {            
            worker.CancelAsync();
            StartButton = Visibility.Visible;
            StopButton = Visibility.Collapsed;
        }

        public void CopyFromClipboard()
        {
            if (GetImage())
            {
                ScreenShotViewModel screenShotViewModel = new ScreenShotViewModel(bitImage, ImageFromClip, this);
            }                
        }

        private bool GetImage()
        { 
            try
            {
                bitImage = BitmapFromSource(Clipboard.GetImage());
                ImageFromClip = Clipboard.GetImage();
                return true;
            }
            catch (System.ArgumentNullException)
            {
                MessageBox.Show("Do a printscreen first!");
                return false;
            }
        }

        public static BitmapSource ConvertBitmap(Bitmap source)
        {
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                          source.GetHbitmap(),
                          IntPtr.Zero,
                          Int32Rect.Empty,
                          BitmapSizeOptions.FromEmptyOptions());
        }

        

        private void CaptureFromScreen()
        {
            Console.WriteLine("one time...");
            using (Bitmap bmpScreenCapture = new Bitmap(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width,
                                            System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bmpScreenCapture))
                {
                    g.CopyFromScreen(0, 0, 0, 0, bmpScreenCapture.Size);
                    bmpScreenCapture.Save(Directory.GetCurrentDirectory() + "ComparedImage.png");

                    Bitmap bm = new Bitmap(bmpScreenCapture);
                    Bitmap croppedComparerImage = bm.Clone(new System.Drawing.Rectangle(CanvasLeft, CanvasTop, RectWidth, RectHeight), bm.PixelFormat);
                    comparedBitmap = croppedComparerImage;

                    Application.Current.Dispatcher.Invoke(new Action(
                        () =>
                        {
                            ComparedImage = ConvertBitmap(croppedComparerImage);

                            if (compare(croppedBitmap, comparedBitmap))
                            {
                                Background = new SolidColorBrush(System.Windows.Media.Colors.DarkSeaGreen);
                            }
                            else
                            {
                                Background = new SolidColorBrush(System.Windows.Media.Colors.Firebrick);
                            }
                        }
                        ));                    
                }
            }
        }

        public static Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            Bitmap bitmap;
            using (var outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new Bitmap(outStream);
            }
            return bitmap;
        }

        private bool compare(Bitmap bmp1, Bitmap bmp2)
        {
            bool equals = true;
            bool flag = true;  //Inner loop isn't broken

            //Test to see if we have the same size of image
            if (bmp1.Size == bmp2.Size)
            {
                for (int x = 0; x < bmp1.Width; ++x)
                {
                    for (int y = 0; y < bmp1.Height; ++y)
                    {
                        if (bmp1.GetPixel(x, y) != bmp2.GetPixel(x, y))
                        {
                            equals = false;
                            flag = false;
                            break;
                        }
                    }
                    if (!flag)
                    {
                        break;
                    }
                }
            }
            else
            {
                equals = false;
            }
            
            if(!equals)
                PlayVoice();

            return equals;
        }

        public void PlayVoice()
        {
                using (SoundPlayer player = new SoundPlayer(Settings.Default.VoiceFile))
                {
                    player.Play();
                }
        }
    }
}
