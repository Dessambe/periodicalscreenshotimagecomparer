﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using PeriodicalScreenShotImageComparer.Commands;
using PeriodicalScreenShotImageComparer.Views;
using System.Drawing;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;

namespace PeriodicalScreenShotImageComparer.ViewModels
{
    public class ScreenShotViewModel : BaseViewModel
    {
        #region Binding: Height and Width of Image
        private int _height;
        public int Height
        {
            get { return _height; }
            set { _height = value; OnPropertyChanged("Height"); }
        }

        private int _width;
        public int Width
        {
            get { return _width; }
            set { _width = value; OnPropertyChanged("Width"); }
        }
        #endregion

        #region Binding: Image
        private ImageSource _imageFromClip;
        public ImageSource ImageFromClip
        {
            get { return _imageFromClip; }
            set { _imageFromClip = value; OnPropertyChanged("ImageFromClip"); }
        }
        #endregion

        #region Binding: Rectangle Dimensions and Visibility
        private Visibility _rectVisibility;
        public Visibility RectVisibility
        {
            get { return _rectVisibility; }
            set { _rectVisibility = value; OnPropertyChanged("RectVisibility"); }
        }

        private int _rectHeight;
        public int RectHeight
        {
            get { return _rectHeight; }
            set { _rectHeight = value; OnPropertyChanged("RectHeight"); }
        }

        private int _rectWidth;
        public int RectWidth
        {
            get { return _rectWidth; }
            set { _rectWidth = value; OnPropertyChanged("RectWidth"); }
        }

        #endregion

        #region Binding: CanvasLeft and CanvasTop
        private int _canvasLeft;
        public int CanvasLeft
        {
            get { return _canvasLeft; }
            set { _canvasLeft = value; OnPropertyChanged("CanvasLeft"); }
        }

        private int _canvasTop;
        public int CanvasTop
        {
            get { return _canvasTop; }
            set { _canvasTop = value; OnPropertyChanged("CanvasTop"); }
        }
        #endregion

        #region Commands
        private ICommand _selectRectangleCommand;
        public ICommand SelectRectangleCommand
        {
            get { return _selectRectangleCommand ?? (_selectRectangleCommand = new BaseCommand(SelectCroppedImage)); }
        }
        #endregion

        #region Fields
        Bitmap bitImage;
        Bitmap croppedImage;
        ListenerViewModel listenerViewModel;
        ScreenShotWindow window;
        #endregion

        public ScreenShotViewModel(Bitmap bitImage, ImageSource ImageFromClip, ListenerViewModel listenerViewModel)
        {
            this.bitImage = bitImage;
            this.ImageFromClip = ImageFromClip;

            Width = (int)ImageFromClip.Width;
            Height = (int)ImageFromClip.Height;

            this.listenerViewModel = listenerViewModel;

            window = new ScreenShotWindow();
            window.DataContext = this;
            window.Show(); 
        }

        private void GetImage() 
        {
            bitImage = BitmapFromSource(Clipboard.GetImage());
            ImageFromClip = Clipboard.GetImage();

            try
            {
                Height = (int)ImageFromClip.Height;
                Width = (int)ImageFromClip.Width;
                Console.WriteLine("Height of image: " + Height);
                Console.WriteLine("Width of image: " + Width);
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Do a printscreen first!");
            }
        }

        private void SelectCroppedImage()
        {
            Bitmap bit = new Bitmap(bitImage);
            croppedImage = bit.Clone(new System.Drawing.Rectangle(CanvasLeft, CanvasTop, RectWidth, RectHeight), bitImage.PixelFormat);
            croppedImage.Save(Directory.GetCurrentDirectory() + "CroppedImage.png");
            CloseScreenShotWindow();
            SendImageToListenerWindow();
        }

        private void CloseScreenShotWindow()
        {
            window.Close();
        }

        private void SendImageToListenerWindow()
        {
            listenerViewModel.GetCroppedImage(croppedImage, CanvasLeft, CanvasTop, RectWidth, RectHeight);
        }

        public static BitmapSource ConvertBitmap(Bitmap source)
        {
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                          source.GetHbitmap(),
                          IntPtr.Zero,
                          Int32Rect.Empty,
                          BitmapSizeOptions.FromEmptyOptions());
        }

        public static Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            Bitmap bitmap;
            using (var outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new Bitmap(outStream);
            }
            return bitmap;
        }
    }
}
