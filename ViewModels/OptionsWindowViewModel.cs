﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using PeriodicalScreenShotImageComparer.Commands;
using System.Windows;
using PeriodicalScreenShotImageComparer.Properties;
using System.IO;

namespace PeriodicalScreenShotImageComparer.ViewModels
{
    public class OptionsWindowViewModel : BaseViewModel
    {
        private ICommand _openDialogCommand;
        public ICommand OpenDialogCommand
        {
            get { return _openDialogCommand ?? (_openDialogCommand = new BaseCommand(OpenDialog)); }
        }

        private ICommand _playSound;
        public ICommand PlaySound
        {
            get { return _playSound ?? (_playSound = new BaseCommand(listenerViewModel.PlayVoice)); }
        }

        private ICommand _closeWindowCommand;
        public ICommand CloseWindowCommand
        {
            get { return _closeWindowCommand ?? (_closeWindowCommand = new BaseCommand(CloseWindow)); }
        }

        private ICommand _saveChangesCommand;
        public ICommand SaveChangesCommand
        {
            get { return _saveChangesCommand ?? (_saveChangesCommand = new BaseCommand(SaveChanges)); }
        }

        ListenerViewModel listenerViewModel;

        public OptionsWindowViewModel(ListenerViewModel listenerViewModel)
        {
            this.listenerViewModel = listenerViewModel;
        }

        private void OpenDialog()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            dlg.DefaultExt = ".wav";
            dlg.Filter = "Wav Files (*.wav)|*.wav";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                Settings.Default.VoiceFile = dlg.FileName;
            }
        }

        private void CloseWindow()
        {
            Application.Current.Windows.Cast<Window>().Single(x => x.IsActive).Close();
        }

        private void SaveChanges()
        {
            Settings.Default.Save();
            CloseWindow();
        }
    }
}
